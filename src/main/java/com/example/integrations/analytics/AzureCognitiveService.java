package com.example.integrations.analytics;

import com.azure.ai.textanalytics.TextAnalyticsClient;
import com.azure.ai.textanalytics.TextAnalyticsClientBuilder;
import com.azure.ai.textanalytics.models.DetectedLanguage;
import com.azure.ai.textanalytics.models.DocumentSentiment;
import com.azure.ai.textanalytics.models.SentenceSentiment;
import com.azure.core.credential.AzureKeyCredential;
import com.example.integrations.models.DocumentText;
import com.example.integrations.models.Documents;
import com.example.integrations.models.SentimentDocs;
import com.example.integrations.models.TweetModel;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.net.ProxySelector;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;

/**
 * Service to connect to Azure Cognitive APIs
 */
@Service
public class AzureCognitiveService {
    public static final String KEY = "8c2084d5df9347138b43fc218010fb3d";
    public static final String AZURE_ENDPOINT = "https://ta-nor-service.cognitiveservices.azure.com";
    public static final String API_KEY_HEADER = "Ocp-Apim-Subscription-Key";

    private TextAnalyticsClient client;


    public AzureCognitiveService() {
        this.client = authenticateClient(KEY, AZURE_ENDPOINT);
    }

    private TextAnalyticsClient authenticateClient(String key, String endpoint) {
        return new TextAnalyticsClientBuilder()
                .credential(new AzureKeyCredential(key))
                .endpoint(endpoint)
                .buildClient();
    }

    public void sentimentAnalysisExample() {
        // The text that need be analyzed.
        String text = "I had the best day of my life. I wish you were there with me.";

        DocumentSentiment documentSentiment = client.analyzeSentiment(text);

        System.out.printf(
                "Recognized document sentiment: %s, positive score: %s, neutral score: %s, negative score: %s.%n",
                documentSentiment.getSentiment(),
                documentSentiment.getConfidenceScores().getPositive(),
                documentSentiment.getConfidenceScores().getNeutral(),
                documentSentiment.getConfidenceScores().getNegative());

        for (SentenceSentiment sentenceSentiment : documentSentiment.getSentences()) {
            System.out.printf(
                    "Recognized sentence sentiment: %s, positive score: %s, neutral score: %s, negative score: %s.%n",
                    sentenceSentiment.getSentiment(),
                    sentenceSentiment.getConfidenceScores().getPositive(),
                    sentenceSentiment.getConfidenceScores().getNeutral(),
                    sentenceSentiment.getConfidenceScores().getNegative());
        }
    }

    public void detectLanguageExample()
    {
        String text = "Тестируем русский текст, потому что знаем русский язык.";

//        TextAnalyticsClient client = authenticateClient(KEY, ENDPOINT);
        DetectedLanguage detectedLanguage = client.detectLanguage(text);
        System.out.printf("Detected primary language: %s, ISO 6391 name: %s, score: %.2f.%n",
                detectedLanguage.getName(),
                detectedLanguage.getIso6391Name(),
                detectedLanguage.getConfidenceScore());

        //Output: Detected primary language: Russian, ISO 6391 name: ru, score: 1.00.
    }

    //JAVA 11 Http call EXAMPLE
    public void doSentimentAnalyticsJava11HttpCall() {
        Documents documents = new Documents();
        DocumentText doc = new DocumentText("en", "1", "Hello world. This is some input text that I love.");
        documents.addDocument(doc);

        Gson gson = new Gson();
        String json = gson.toJson(documents);

        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .proxy(ProxySelector.getDefault())
                .connectTimeout(Duration.ofSeconds(5))
                .build();

        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .uri(URI.create("https://ta-nor-service.cognitiveservices.azure.com/text/analytics/v3.0/sentiment"))
                .header(API_KEY_HEADER, KEY)
                .build();

        httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .thenAccept(body -> {
                    SentimentDocs sentimentDocs = gson.fromJson(body, SentimentDocs.class);

                    System.out.println("Sentiment is " + sentimentDocs.getDocuments().get(0).getSentiment());
                });

        System.out.println("This will be called first because our call is async.");
    }

    //JAVA 11 Http call Integration with actual object passed into the function
    public CompletableFuture<HttpResponse<String>> doSentimentAnalyticsIntegration(TweetModel tweetModel) {
        Documents documents = new Documents();
        DocumentText doc = new DocumentText("en", tweetModel.getData().getId(), tweetModel.getData().getText());
        documents.addDocument(doc);

        Gson gson = new Gson();
        String json = gson.toJson(documents);

        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .proxy(ProxySelector.getDefault())
                .connectTimeout(Duration.ofSeconds(5))
                .build();

        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .uri(URI.create("https://ta-nor-service.cognitiveservices.azure.com/text/analytics/v3.0/sentiment"))
                .header(API_KEY_HEADER, KEY)
                .build();

        return httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString());
    }

}
