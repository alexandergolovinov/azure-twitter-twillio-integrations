package com.example.integrations.analytics;

import com.example.integrations.models.StreamRuleRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service to connect to Twitter APIs
 * API REFERENCE
 * Docs: https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/api-reference/get-tweets-search-stream#Default
 */
@Service
public class TwitterService {
    public static final String BEARER_TOKEN = "AAAAAAAAAAAAAAAAAAAAAHOVVAEAAAAAoDJthnWyI7aQp2IsXloos1HYCIU%3Dirx9PWbJYgjtDZi72xijZ6Mjr6fga1HII3C6ID3O9c514P1iaf";
    public static final String HEADER_PREFIX = "Bearer ";
    public static final String TWITTER_ENDPOINT_PATH = "/2/tweets/search/recent?query=Self Study and Motivation&max_results=10&tweet.fields=author_id";
    public static final String TWITTER_STREAM_ENDPOINT_PATH = "/2/tweets/search/stream";
    public static final String TWITTER_STREAM_ENDPOINT_RULES = "/2/tweets/search/stream/rules";

    public static final String TWITTER_ENDPOINT = "https://api.twitter.com";

    private final WebClient client;

    public TwitterService() {
        this.client = WebClient.create(TWITTER_ENDPOINT);
    }

    public void getRecentTweetsSearch() {
        WebClient client = WebClient.create(TWITTER_ENDPOINT);

        Mono<ResponseEntity<String>> mono = client
                .get()
                .uri(TWITTER_ENDPOINT_PATH)
                .header("Authorization", HEADER_PREFIX + BEARER_TOKEN)
                .retrieve()
                .toEntity(String.class);

        // Synchronouse. Block will stop execution and wait the API response here before proceed. Blocking other threads.
        // -> ResponseEntity<String> response = mono.block();

        // Asynchronouse. Executing on a different thread. Non blocking.
        mono.subscribe(res -> {
            System.out.println("Executed second " + res);
        });

        System.out.println("Executed first");
    }

    /**
     * Create RULES for Streaming API
     */
    public void getRecentTweetsStream() {
        StreamRuleRequest ruleRequest = new StreamRuleRequest();
        ruleRequest.addRule("LinkedIn", "LinkedIn Tag");

        client.post()
                .uri(TWITTER_STREAM_ENDPOINT_RULES)
                .header("Authorization", HEADER_PREFIX + BEARER_TOKEN)
                .bodyValue(ruleRequest)
                .retrieve()
                .toBodilessEntity()
                .subscribe(response -> {
                    //Connect to a Stream
                   tweetStream()
                           .subscribe(System.out::println);

                });
    }

    /**
     * Stream filtered Tweets
     */
    public Flux<String> tweetStream() {
        return client.get()
                .uri(TWITTER_STREAM_ENDPOINT_PATH)
                .header("Authorization", HEADER_PREFIX + BEARER_TOKEN)
                .retrieve()
                .bodyToFlux(String.class)
                .filter(body -> !body.isBlank());
    }
}
