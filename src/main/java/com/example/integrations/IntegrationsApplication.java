package com.example.integrations;

import com.example.integrations.analytics.AzureCognitiveService;
import com.example.integrations.analytics.TwillioService;
import com.example.integrations.analytics.TwitterService;
import com.example.integrations.models.SentimentDocs;
import com.example.integrations.models.TweetModel;
import com.google.gson.Gson;
import com.twilio.Twilio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.http.HttpResponse;

import static com.example.integrations.analytics.TwillioService.TWILLIO_SID;
import static com.example.integrations.analytics.TwillioService.TWILLIO_TOKEN;

@SpringBootApplication
public class IntegrationsApplication implements CommandLineRunner {

    private final TwitterService twitterService;
    private final AzureCognitiveService azureCognitiveService;
    private final TwillioService twillioService;

    @Autowired
    public IntegrationsApplication(TwitterService twitterService, AzureCognitiveService azureCognitiveService, TwillioService twillioService) {
        this.twitterService = twitterService;
        this.azureCognitiveService = azureCognitiveService;
        this.twillioService = twillioService;
    }

    public static void main(String[] args) {
        SpringApplication.run(IntegrationsApplication.class, args);
        Twilio.init(TWILLIO_SID, TWILLIO_TOKEN);
    }

    @Override
    public void run(String... args) {
        twitterService.tweetStream()
                .subscribe(tweet -> {
                    //Deserialize the JSON from Twitter into a TweetModel response
                    TweetModel tweetModel = new Gson().fromJson(tweet, TweetModel.class);

                    //Send Tweet into Azure Cognitive Analytics
                    azureCognitiveService.doSentimentAnalyticsIntegration(tweetModel)
                            .thenApply(HttpResponse::body)
                            .thenAccept(body -> {
                                SentimentDocs sentimentDocs = new Gson().fromJson(body, SentimentDocs.class);

                                String positiveSentiment = sentimentDocs.getDocuments().get(0).getSentiment().toUpperCase();
                                System.out.println("Sentiment for tweet ID: " + tweetModel.getData().getId() + "is: " + sentimentDocs.getDocuments().get(0).getSentiment().toUpperCase());

                                //Send SMS if Sentiment is positive
                                if (positiveSentiment.equals("POSITIVE")) {
                                    twillioService.sendSMSMessage();
                                }
                            });
                });
    }
}
