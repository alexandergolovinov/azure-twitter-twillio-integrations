package com.example.integrations.models;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SentimentDocs {
    private List<SentimentAnalysisText> documents = new ArrayList<>();
}
