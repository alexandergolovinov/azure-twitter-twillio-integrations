package com.example.integrations.models;

import lombok.Data;

@Data
public class TweetModel {
    private TweetText data;
}
