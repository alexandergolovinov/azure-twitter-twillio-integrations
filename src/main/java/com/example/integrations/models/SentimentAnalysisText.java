package com.example.integrations.models;

import lombok.Data;

@Data
public class SentimentAnalysisText {
    private String id;
    private String sentiment;
}
