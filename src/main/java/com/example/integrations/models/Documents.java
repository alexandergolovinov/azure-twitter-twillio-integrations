package com.example.integrations.models;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Documents {
    private List<DocumentText> documents = new ArrayList<>();

    public void addDocument(DocumentText doc) {
        this.documents.add(doc);
    }
}
