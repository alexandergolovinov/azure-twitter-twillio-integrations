package com.example.integrations.models;

import lombok.Data;

@Data
public class DocumentText {
    private String language;
    private String id;
    public String text;

    public DocumentText(String language, String id, String text) {
        this.language = language;
        this.id = id;
        this.text = text;
    }
}
