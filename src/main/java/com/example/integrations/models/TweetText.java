package com.example.integrations.models;

import lombok.Data;

@Data
public class TweetText {
    private String id;
    private String text;
}
