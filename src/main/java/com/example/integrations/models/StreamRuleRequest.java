package com.example.integrations.models;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class StreamRuleRequest {
    private List<StreamRule> add = new ArrayList<>();

    public void addRule(String value, String tag) {
        this.add.add(new StreamRule(value, tag));
    }
}
