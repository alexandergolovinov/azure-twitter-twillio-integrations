package com.example.integrations.models;

import lombok.Data;

@Data
public class StreamRule {
    private String value;
    private String tag;

    public StreamRule(String value, String tag) {
        this.value = value;
        this.tag = tag;
    }
}
